# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2021, 2022.
# Aziz Adam Adrian <4.adam.adrian@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2022-07-01 14:51+0700\n"
"Last-Translator: Aziz Adam Adrian <4.adam.adrian@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wantoyo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wantoyek@gmail.com"

#: kdeconnectd.cpp:54
#, kde-format
msgid ""
"Pairing request from %1\n"
"Key: %2..."
msgstr ""
"Permintaan penyandingan dari %1\n"
"Kunci: %2..."

#: kdeconnectd.cpp:55
#, kde-format
msgid "Open"
msgstr "Buka"

#: kdeconnectd.cpp:56
#, kde-format
msgid "Accept"
msgstr "Setujui"

#: kdeconnectd.cpp:56
#, kde-format
msgid "Reject"
msgstr "Tolak"

#: kdeconnectd.cpp:56
#, kde-format
msgid "View key"
msgstr "Tampilan kunci"

#: kdeconnectd.cpp:139 kdeconnectd.cpp:141
#, kde-format
msgid "KDE Connect Daemon"
msgstr "Daemon KDE Connect"

#: kdeconnectd.cpp:147
#, kde-format
msgid "Replace an existing instance"
msgstr "Ganti instansi yang ada"

#: kdeconnectd.cpp:150
#, kde-format
msgid ""
"Launch a private D-Bus daemon with kdeconnectd (macOS test-purpose only)"
msgstr ""
"Luncurkan daemon D-Bus pribadi dengan kdeconnectd (hanya untuk tujuan "
"pengujian macOS)"
